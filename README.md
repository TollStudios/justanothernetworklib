# JustAnotherNetworkLib - indev
[![Nuget (with prereleases)](https://img.shields.io/nuget/vpre/JustAnotherNetworkLib.svg)](https://www.nuget.org/packages/JustAnotherNetworkLib/)
[![Nuget](https://img.shields.io/nuget/dt/JustAnotherNetworkLib.svg)](https://www.nuget.org/packages/JustAnotherNetworkLib/)  

![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=justanothernetworklib&metric=alert_status)
![Coverage](https://sonarcloud.io/api/project_badges/measure?project=justanothernetworklib&metric=coverage)
  
Lite, simple and reliable UDP library build upon [LiteNetLib](https://github.com/RevenantX/LiteNetLib) for .NET Standard 2.0  

## Builds
- [NuGet](https://www.nuget.org/packages/JustAnotherNetworkLib/)
- [Releases](https://gitlab.com/TollStudios/justanothernetworklib/tags)

For more Information about the Changes check out the [Changelog](https://gitlab.com/TollStudios/justanothernetworklib/blob/master/CHANGELOG.md)

## Features
- Lightweight (small CPU and RAM usage)
- Relatively small Packages
- Simple connection handling
- Simple Package-Sending mechanism
- Automatic Package compression
- Usable for Unity and all other C# Projects


## Usage Examples

```c#
// Create a new NetClient with a specific connection-key
m_client = new NetClient("Example");
// Create a new NetServer with a specific connection-key and a max posibble clients of 1.
m_server = new NetServer("Example", 1)
{
    Port = 1337
};

// the connection-keys have to be the same, otherwise a connection cannot be established!

// hook up some Events
m_server.EventListener.ClientStarted += () =>
{
    Console.WriteLine($"Server successfully started on Port {m_server.Port}");
};

m_client.EventListener.ClientStarted += () =>
{
    Console.WriteLine("Client successfully started");
};

m_server.EventListener.PeerConnected += (peer) =>
{
    Console.WriteLine($"We have a new client: {peer.EndPoint} - ID = {peer.ConnectId}");
};

m_server.EventListener.NetworkReceiveData += (peer, packet, packetSize) =>
{
    Console.WriteLine($"We received data from {peer.EndPoint} with a size of {packetSize}B - ID = {peer.ConnectId}");
    Console.WriteLine($"Message-Type = {packet.Type} | Message-Content = {packet.Payload.ToString()}");
};

// start both the server and the client
m_server.Start();
m_client.Start();

// connect the client to the server
m_client.Connect("localhost", 1337);

// send a message to the server
// The Payload (aka Message-Content) can be any serializable data (any default data-type and everything that is marked with [System.Serializable])
m_client.SendMessage(1, "Hey this is a message from the client!");

// We have to wait a bit for the server to evaluate the messages
System.Threading.Thread.Sleep(20);

// Disconnect all clients from the server - you can provide an optional reason why the connection was closed
m_server.CloseConnection("Server is shutting down");

// Stop both the server and the client
m_server.Stop();
m_client.Stop();

Console.WriteLine("\n\n\nPress any Key to Close the program...");
Console.ReadKey();

```

## Contribute

Want to contribute to this awesome Project? Check out the [Contribution-Guidlines](https://gitlab.com/TollStudios/justanothernetworklib/blob/master/CONTRIBUTING.md)!