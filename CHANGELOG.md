# Changelog


### 0.1.2 - 01.12.2018
- Internal changes that unified most of the Function Calls. New Functions: `Connect()`, `CloseConnection()` and `SendMessage()` for both Server and Client
- [+ New and easier way to send a message: SendMessage(byte id, object payload) +]
- [+ New functions to retrieve more infos: GetPeers(), GetFirstPeer(), GetStatistics() +]
- [+ New Event for a Disconnect with reason: EventListener.PeerDisconnectedWithReason. Can be subscribed for both Server and Client +]

### 0.1.1 - 27.09.2018
- [+ added the ability to provide an additional disconnect-reason +]
- Can be retrieved by calling `DisconnectInfo.AdditionalInfo.GetString()` in `NetServer` or `NetClient`

### 0.1.0 - 26.09.2018
- [- fixed some errors in the ´Utils.DataCompressionHelper´ regarding null-values -]
- [+ added an Example Project to show how that stuff works +]

### 0.0.3 - 25.09.2018
- [+ changed Meta-Data of the NuGet-Package +]

### 0.0.2 - 25.09.2018
- [+ changed Meta-Data of the NuGet-Package +]

### 0.0.1 - 25.09.2018
- [+ added the initial-source-code +]