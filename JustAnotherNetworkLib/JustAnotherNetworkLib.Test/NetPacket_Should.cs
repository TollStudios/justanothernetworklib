﻿using FluentAssertions;
using JustAnotherNetworkLib.Model;
using NEdifis;
using NEdifis.Attributes;
using NUnit.Framework;

namespace JustAnotherNetworkLib.Test
{
    [TestFixtureFor(typeof(NetPacket))]
    public class NetPacket_Should
    {
        [Test]
        public void Be_Creatable()
        {
            const byte type = 0b0101_1010;
            const string payload = "Test123";

            var sut = new NetPacket(type, payload);

            sut.Should().NotBeNull();
            sut.Type.Should().Be(type);
            sut.Payload.Should().Be(payload);
        }

        [Test]
        public void Be_Creatable_Standard_Constructor()
        {
            var sut = new NetPacket();

            sut.Should().NotBeNull();
            sut.Type.Should().NotBe(null);
            sut.Payload.Should().BeNull();
        }

        [Test]
        public void Fail_Serialize_With_Not_Serializable_Class()
        {
            var sut = new NetPacket(0b1010_0101, new NotSerializableClass());

            System.Exception exception = null;
            byte[] data = null;

            try
            {
                data = sut.Serialize();
            }
            catch (System.Exception ex)
            {
                exception = ex;
            }

            data.Should().BeNull();
            exception.Should().NotBeNull();
            exception.GetType().Should().Be(typeof(System.ArgumentException));
        }

        [Test]
        public void Fail_Serialize_With_Null_Payload()
        {
            var sut = new NetPacket(0b1010_0101, null);

            System.Exception exception = null;
            byte[] data = null;

            try
            {
                data = sut.Serialize();
            }
            catch (System.Exception ex)
            {
                exception = ex;
            }

            data.Should().BeNull();
            exception.Should().NotBeNull();
            exception.GetType().Should().Be(typeof(System.ArgumentNullException));
        }

        [Test]
        [TestCase("Test123")]
        [TestCase(1.234f)]
        [TestCase(1234)]
        [TestCase(1.234)]
        [TestCase((short)1234)]
        [TestCase((long)1234)]
        public void Serialize_With_Regular_Data(object data)
        {
            var sut = new NetPacket(0b0101_1010, data);

            byte[] serializedData = sut.Serialize();

            serializedData.Should().NotBeNullOrEmpty();
        }

        [Test]
        public void Fail_Serialize_With_Null_Data()
        {
            var ctx = new ContextFor<NetPacket>();
            var sut = ctx.BuildSut();

            System.Exception exception = null;

            try
            {
                sut.Deserialize(null);
            }
            catch (System.Exception ex)
            {
                exception = ex;
            }

            exception.Should().NotBeNull();
            exception.GetType().Should().Be(typeof(System.ArgumentNullException));
        }

        [Test]
        public void Deserialize_With_Data_With_Length_One()
        {
            var ctx = new ContextFor<NetPacket>();
            var sut = ctx.BuildSut();

            var sut2 = sut.Deserialize(new byte[] { 0b0101_1010 });

            sut2.Should().NotBeNull();
            sut.Type.Should().NotBe(null);
            sut.Payload.Should().BeNull();
        }

        [Test]
        [TestCase(0b0000_0000, "Test123")]
        [TestCase(0b0101_1010, 1.234f)]
        [TestCase(0b1111_1111, (long)1234)]
        public void Serialize_And_Deserialize_Succeed(byte testType, object testPayload)
        {
            var sut = new NetPacket(testType, testPayload);

            var data = sut.Serialize();
            var sut2 = sut.Deserialize(data);

            sut.Should().NotBeNull();
            sut.Type.Should().Be(testType);
            sut.Payload.Should().Be(testPayload);

            sut2.Should().NotBeNull();
            sut2.Type.Should().Be(testType);
            sut2.Payload.Should().Be(testPayload);
        }

        [Test]
        public void Equals_Fail_With_Null()
        {
            var ctx = new ContextFor<NetPacket>();
            var sut = ctx.BuildSut();

            sut.Equals(null).Should().Be(false);
        }

        [Test]
        public void Equals_Fail_With_Other_Type()
        {
            var ctx = new ContextFor<NetPacket>();
            var sut = ctx.BuildSut();

            sut.Equals("123").Should().Be(false);
        }
    }

    [ExcludeFromConventions("Just a Test-Helper-Class")]
    public class NotSerializableClass
    {
        public int someNumber;
        public string someString;
    }
}