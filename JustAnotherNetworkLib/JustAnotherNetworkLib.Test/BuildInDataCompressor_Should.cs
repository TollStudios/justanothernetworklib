﻿using System;
using FluentAssertions;
using JustAnotherNetworkLib.Utils;
using NEdifis;
using NEdifis.Attributes;
using NUnit.Framework;

namespace JustAnotherNetworkLib.Test
{
    [TestFixtureFor(typeof(BuildInDataCompressor))]
    public class BuildInDataCompressor_Should
    {
        [Test]
        public void Be_Creatable()
        {
            var ctx = new ContextFor<BuildInDataCompressor>();
            var sut = ctx.BuildSut();

            sut.Should().NotBeNull();
        }

        [Test]
        public void Compress_Valid_Data()
        {
            var ctx = new ContextFor<BuildInDataCompressor>();
            var sut = ctx.BuildSut();

            byte[] result = sut.Compress("Test123");

            result.Should().NotBeNullOrEmpty();
        }

        [Test]
        public void Compress_Null_Data_And_Throw_Exception()
        {
            var ctx = new ContextFor<BuildInDataCompressor>();
            var sut = ctx.BuildSut();

            byte[] data = null;

            Assert.Throws<ArgumentNullException>(() => data = sut.Compress<object>(null));
            data.Should().BeNullOrEmpty();
        }

        [Test]
        [TestCase("Toll")]
        [TestCase("Test123")]
        [TestCase(1.234f)]
        [TestCase(1.234)]
        [TestCase((long)1234)]
        [TestCase(1234)]
        [TestCase((short)1234)]
        public void Compress_And_Decompress_Valid_Data(object testData)
        {
            var ctx = new ContextFor<BuildInDataCompressor>();
            var sut = ctx.BuildSut();

            var data = sut.Compress(testData);

            sut.Decompress<object>(data).Should().Be(testData);
        }

        [Test]
        public void Decompress_Null_Data_And_Throw_Exception()
        {
            var ctx = new ContextFor<BuildInDataCompressor>();
            var sut = ctx.BuildSut();

            byte[] data = null;

            Assert.Throws<ArgumentNullException>(() => sut.Decompress<object>(null));
            data.Should().BeNullOrEmpty();
        }
    }
}