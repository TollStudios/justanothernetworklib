﻿using System;
using FluentAssertions;
using JustAnotherNetworkLib.Utils;
using NEdifis;
using NEdifis.Attributes;
using NUnit.Framework;

namespace JustAnotherNetworkLib.Test
{
    [TestFixtureFor(typeof(ObjectConverter))]
    public class ObjectConverter_Should
    {
        [Test]
        public void Be_Creatable()
        {
            var ctx = new ContextFor<ObjectConverter>();
            var sut = ctx.BuildSut();

            sut.Should().NotBeNull();
        }

        [Test]
        [TestCase("Toll")]
        [TestCase("Test123")]
        [TestCase(1.234f)]
        [TestCase(1.234)]
        [TestCase((long)1234)]
        [TestCase(1234)]
        [TestCase((short)1234)]
        public void Convert_From_And_To_Byte_Array(object testData)
        {
            var ctx = new ContextFor<ObjectConverter>();
            var sut = ctx.BuildSut();

            sut.FromByteArray<object>(sut.ToByteArray(testData)).Should().Be(testData);
        }

        [Test]
        public void Fail_On_Null_Data()
        {
            var ctx = new ContextFor<ObjectConverter>();
            var sut = ctx.BuildSut();

            Assert.Throws<ArgumentNullException>(() => sut.FromByteArray<object>(null));
            Assert.Throws<ArgumentNullException>(() => sut.ToByteArray<object>(null));
        }
    }
}
