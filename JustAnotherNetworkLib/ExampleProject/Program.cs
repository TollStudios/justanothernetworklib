﻿using JustAnotherNetworkLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExampleProject
{
    internal static class Program
    {
        private static NetClient m_client;
        private static NetServer m_server;

        internal static void Main(string[] args)
        {
            if (args == null)
            {
                throw new ArgumentNullException(nameof(args));
            }

            // Create a new NetClient with a specific connection-key
            m_client = new NetClient("Example");
            // Create a new NetServer with a specific connection-key and a max posibble clients of 1.
            m_server = new NetServer("Example", 1)
            {
                Port = 1337
            };

            // the connection-keys have to be the same, otherwise a connection cannot be established!

            // hook up some Events
            m_server.EventListener.ClientStarted += () => Console.WriteLine($"Server successfully started on Port {m_server.Port}");

            m_client.EventListener.ClientStarted += () => Console.WriteLine("Client successfully started");

            m_server.EventListener.PeerConnected += (peer) => Console.WriteLine($"We have a new client: {peer.EndPoint} - ID = {peer.Id}");

            m_server.EventListener.NetworkReceiveData += (peer, packet, packetSize) =>
            {
                Console.WriteLine($"We received data from {peer.EndPoint} with a size of {packetSize}B - ID = {peer.Id}");
                Console.WriteLine($"Message-Type = {packet.Type} | Message-Content = {packet.Payload}");
            };

            // start both the server and the client
            m_server.Start();
            m_client.Start();

            // connect the client to the server
            m_client.Connect("localhost", 1337);

            // send a message to the server
            // The Payload (aka Message-Content) can be any serializable data (any default data-type and everything that is marked with [System.Serializable])
            m_client.SendMessage(1, "Hey this is a message from the client!");

            // We have to wait a bit for the server to evaluate the messages
            System.Threading.Thread.Sleep(20);

            // Disconnect all clients from the server - you can provide an optional reason why the connection was closed
            m_server.CloseConnection("Server is shutting down");

            // Stop both the server and the client
            m_server.Stop();
            m_client.Stop();

            Console.WriteLine("\n\n\nPress any Key to Close the program...");
            Console.ReadKey();
        }
    }
}