﻿using JustAnotherNetworkLib.Contracts;

namespace JustAnotherNetworkLib.Utils
{
    /// <summary>
    /// Just an EmptyDataCompressor that returns Data >as is<
    /// Will be needed depending on the Configuration
    /// </summary>
    public class EmptyDataCompressor : IDataCompressor
    {
        private IObjectConverter m_converter;

        public EmptyDataCompressor()
        {
            m_converter = new ObjectConverter();
        }

        public byte[] Compress<T>(T data)
        {
            return m_converter.ToByteArray(data);
        }

        public T Decompress<T>(byte[] data)
        {
            return m_converter.FromByteArray<T>(data);
        }
    }
}
