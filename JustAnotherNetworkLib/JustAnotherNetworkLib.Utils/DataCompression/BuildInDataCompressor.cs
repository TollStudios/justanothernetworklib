﻿using System;
using System.IO;
using System.IO.Compression;
using JustAnotherNetworkLib.Contracts;

namespace JustAnotherNetworkLib.Utils
{
    /// <summary>
    /// Compressor to compress and decompress data with the build in systems
    /// 
    /// <para>
    /// CAUTION: This class will not check for integrity of the data! Corruption may be possible
    /// </para>
    /// </summary>
    public class BuildInDataCompressor : IDataCompressor
    {
        private readonly IObjectConverter m_objectConverter;

        public BuildInDataCompressor()
        {
            m_objectConverter = new ObjectConverter();
        }

        /// <summary>
        /// Compresses data as a byte-array and returns the compressed data as a byte-array
        /// </summary>
        /// <param name="data">The data to compress as a byte-array</param>
        /// <returns>The compressed data as a byte-array. Could return null if something is wrong</returns>
        public byte[] Compress<T>(T data)
        {
            if (data == null) { throw new ArgumentNullException(nameof(data)); }

            var convertedData = m_objectConverter.ToByteArray(data);

            var output = new MemoryStream();
            using (var dstream = new DeflateStream(output, CompressionLevel.Optimal))
            {
                dstream.Write(convertedData, 0, convertedData.Length);
            }
            return output.ToArray();
        }

        /// <summary>
        /// Decompresses previously compressed data and returns the decompressed data as a byte-array
        /// </summary>
        /// <param name="data">Previously compressed data as a byte-array</param>
        /// <returns>The decompressed data as a byte-array</returns>
        public T Decompress<T>(byte[] data)
        {
            if (data == null) { throw new ArgumentNullException(nameof(data)); }

            var input = new MemoryStream(data);
            var output = new MemoryStream();

            using (var dstream = new DeflateStream(input, CompressionMode.Decompress))
            {
                dstream.CopyTo(output);
            }

            return m_objectConverter.FromByteArray<T>(output.ToArray());
        }
    }
}