﻿using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using JustAnotherNetworkLib.Contracts;

namespace JustAnotherNetworkLib.Utils
{
    /// <summary>
    /// Helper-Class to Convert from and to Binary-Arrays
    /// </summary>
    public class ObjectConverter : IObjectConverter
    {
        public ObjectConverter() { }

        /// <summary>
        /// Convert given data into a Byte-Array
        /// </summary>
        /// <typeparam name="T">The Type of the given data</typeparam>
        /// <param name="data">The data to convert</param>
        /// <returns>The converted data as a Byte-Array</returns>
        /// <exception cref="ArgumentNullException"/>
        public byte[] ToByteArray<T>(T data)
        {
            if (data == null) throw new ArgumentNullException(nameof(data));

            var bf = new BinaryFormatter();
            using (var ms = new MemoryStream())
            {
                bf.Serialize(ms, data);
                return ms.ToArray();
            }
        }

        /// <summary>
        /// Convert the given Byte-Array into the wanted Type
        /// </summary>
        /// <typeparam name="T">The Type to convert to</typeparam>
        /// <param name="data">The data to convert</param>
        /// <returns>The converted data as the given Type</returns>
        /// <exception cref="ArgumentException"/>
        public T FromByteArray<T>(byte[] data)
        {
            if (data == null) throw new ArgumentNullException(nameof(data));

            var bf = new BinaryFormatter();
            using (var ms = new MemoryStream(data))
            {
                object obj = bf.Deserialize(ms);
                return (T)obj;
            }
        }
    }
}
