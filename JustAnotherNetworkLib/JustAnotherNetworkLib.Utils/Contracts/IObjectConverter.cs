﻿namespace JustAnotherNetworkLib.Contracts
{
    public interface IObjectConverter
    {
        byte[] ToByteArray<T>(T data);
        T FromByteArray<T>(byte[] data);
    }
}
