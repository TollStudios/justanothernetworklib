﻿namespace JustAnotherNetworkLib.Contracts
{
    public interface IDataCompressor
    {
        byte[] Compress<T>(T data);

        T Decompress<T>(byte[] data);
    }
}