﻿using System.Net;
using System.Net.Sockets;
using JustAnotherNetworkLib.Utils;
using LiteNetLib;
using JustAnotherNetworkLib.Contracts;
using JustAnotherNetworkLib.Model;

namespace JustAnotherNetworkLib
{
    /// <summary>
    /// A simple class that "Converts" the Callback-Functions from the INetEventListener-Interface into C#-Events
    /// </summary>
    public class NetEventListener : INetEventListener
    {
        public delegate void ClientStartedEventHandler();
        public delegate void ClientStoppedEventHandler();
        public delegate void NetworkErrorEventHandler(IPEndPoint endPoint, SocketError socketError);
        public delegate void NetworkLatencyUpdateEventHandler(NetPeer peer, int latency);
        public delegate void NetworkReceiveDataEventHandler(NetPeer peer, INetPacket packet, int packetSize);
        public delegate void NetworkReceiveConnectionRequestEventHandler(ConnectionRequest connectionRequest);
        public delegate void NetworkReceiveUnconnectedEventHandler(IPEndPoint remoteEndPoint, NetPacketReader reader, UnconnectedMessageType messageType);
        public delegate void PeerConnectedEventHandler(NetPeer peer);
        public delegate void PeerDisconnectedEventHandler(NetPeer peer, DisconnectInfo disconnectInfo);
        public delegate void PeerDisconnectedWithReasonEventHandler(NetPeer peer, string reason, DisconnectInfo disconnectInfo);

        public event ClientStartedEventHandler ClientStarted;
        public event ClientStoppedEventHandler ClientStopped;
        public event NetworkErrorEventHandler NetworkError;
        public event NetworkLatencyUpdateEventHandler NetworkLatencyUpdate;
        public event NetworkReceiveDataEventHandler NetworkReceiveData;
        public event NetworkReceiveConnectionRequestEventHandler NetworkConnectionRequest;
        public event NetworkReceiveUnconnectedEventHandler NetworkReceiveUnconnected;
        public event PeerConnectedEventHandler PeerConnected;
        public event PeerDisconnectedEventHandler PeerDisconnected;
        public event PeerDisconnectedWithReasonEventHandler PeerDisconnectedWithReason;

        private readonly IDataCompressor m_compressor;

        /// <summary>
        /// Standard-Constructor for creating an Object of this Class
        /// </summary>
        public NetEventListener()
        {
            m_compressor = new BuildInDataCompressor();
        }

        /// <summary>
        /// Will be called when the underlying Client or Server has started
        /// </summary>
        public void OnClientStarted()
        {
            ClientStarted?.Invoke();
        }

        /// <summary>
        /// Will be called when the underlying Client or Server has stopped
        /// </summary>
        public void OnClientStopped()
        {
            ClientStopped?.Invoke();
        }

        /// <summary>
        /// Will be called when the underlying Client or Server has received an network-error
        /// </summary>
        /// <param name="endPoint">The Endpoint that caused the network-error</param>
        /// <param name="socketError">The socket-error for the caused network-error</param>
        public void OnNetworkError(IPEndPoint endPoint, SocketError socketError)
        {
            NetworkError?.Invoke(endPoint, socketError);
        }

        /// <summary>
        /// Will be called when the underlying Client or Server has updated the latency-information for a connected Peer
        /// </summary>
        /// <param name="peer">The NetPeer for which the information was updated</param>
        /// <param name="latency">The latency-amount that was calculated</param>
        public void OnNetworkLatencyUpdate(NetPeer peer, int latency)
        {
            NetworkLatencyUpdate?.Invoke(peer, latency);
        }

        /// <summary>
        /// Will be called when the underlying Client or Server has received a new Message with data from a Peer that is connected to the Server or Client
        /// </summary>
        /// <param name="peer">The Peer that has sent the Data</param>
        /// <param name="reader">A NetDataReader-Object that contains the data that has been send</param>
        public void OnNetworkReceive(NetPeer peer, NetPacketReader reader, DeliveryMethod deliveryMethod)
        {
            int packetSize = reader.AvailableBytes;

            byte[] data = new byte[packetSize];
            reader.GetBytes(data, packetSize);

            NetworkReceiveData?.Invoke(peer, new NetPacket().Deserialize(m_compressor.Decompress<byte[]>(data)), packetSize);
        }

        /// <summary>
        /// Will be called when the underlying Client or Server has received a Connection-Request from another client/server
        /// </summary>
        /// <param name="request">Class containing data for the request and functions to accept the request</param>
        public void OnConnectionRequest(ConnectionRequest request)
        {
            NetworkConnectionRequest?.Invoke(request);
        }

        /// <summary>
        /// Will be called when the underlying Client or Server has received a new Message with data from a Peer that is NOT connected to the Server or Client
        /// (i.e. Discovery-Request)
        /// </summary>
        /// <param name="remoteEndPoint">The EndPoint that has sent the data</param>
        /// <param name="reader">A NetDataReader-Object that containts the data that has been send</param>
        /// <param name="messageType">The Type of the received Message</param>
        public void OnNetworkReceiveUnconnected(IPEndPoint remoteEndPoint, NetPacketReader reader, UnconnectedMessageType messageType)
        {
            NetworkReceiveUnconnected?.Invoke(remoteEndPoint, reader, messageType);
        }

        /// <summary>
        /// Will be called when the underlying Client or Server has a new connected Peer
        /// </summary>
        /// <param name="peer">The NetPeer that has connected</param>
        public void OnPeerConnected(NetPeer peer)
        {
            PeerConnected?.Invoke(peer);
        }

        /// <summary>
        /// Will be called when the underlying Client or Server has a new disconnected Peer
        /// </summary>
        /// <param name="peer">The peer that disconnected from the Server or Client</param>
        /// <param name="disconnectInfo">Information about the Disconnect</param>
        public void OnPeerDisconnected(NetPeer peer, DisconnectInfo disconnectInfo)
        {
            if (disconnectInfo.Reason == DisconnectReason.RemoteConnectionClose)
            {
                string reason = disconnectInfo.AdditionalData.GetString();

                if (reason != string.Empty)
                {
                    OnPeerDisconnectedWithReason(peer, reason, disconnectInfo);
                    return;
                }
            }

            PeerDisconnected?.Invoke(peer, disconnectInfo);
        }

        /// <summary>
        /// Will be called when the underlying Client or Server has a new disconnected Peer, that provided a Disconnect-Reason
        /// </summary>
        /// <param name="peer">The peer that disconnected from the Server or Client</param>
        /// <param name="reason">A written Reason why the Peer was disconnected</param>
        /// <param name="disconnectInfo">Information about the Disconnect</param>
        public void OnPeerDisconnectedWithReason(NetPeer peer, string reason, DisconnectInfo disconnectInfo)
        {
            PeerDisconnectedWithReason?.Invoke(peer, reason, disconnectInfo);
        }
    }
}