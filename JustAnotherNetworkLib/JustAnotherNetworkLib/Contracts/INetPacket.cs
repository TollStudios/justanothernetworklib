﻿namespace JustAnotherNetworkLib.Contracts
{
    public interface INetPacket
    {
        byte Type { get; }
        object Payload { get; }

        byte[] Serialize();
        INetPacket Deserialize(byte[] data);
    }
}