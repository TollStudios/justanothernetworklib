﻿using LiteNetLib;
using JustAnotherNetworkLib.Model;

namespace JustAnotherNetworkLib
{
    /// <summary>
    /// Class to handle the connection to a Server as a Client
    /// </summary>
    public class NetClient : NetManagerBase
    {
        /// <summary>
        /// If this Client is able to connect to more than one server
        /// </summary>
        private readonly bool m_allowMultipleServerConnections;

        /// <summary>
        /// Create a new NetClient-Object with all the needed Information
        /// </summary>
        /// <param name="connectKey">A Connection-Key that has to be the same on the client and on the Server. Else a connection will not be made.</param>
        /// <param name="allowMultipleServerConnections">[OPTIONAL] Allow the client to connect to more than one server. Is set to false by default. SHOULD STAY FALSE</param>
        /// <param name="updateTime">[OPTIONAL] Sets the interval in which the logic of the underlying Library will be updated in Milliseconds. Is set to 15ms by default.</param>
        /// <returns></returns>
        public NetClient(string connectKey, bool allowMultipleServerConnections = false, int updateTime = 15) : base(connectKey, updateTime, false)
        {
            m_allowMultipleServerConnections = allowMultipleServerConnections;

            m_manager = new NetManager(EventListener)
            {
                UpdateTime = updateTime
            };
        }

        /// <summary>
        /// <para>
        /// Connects this client to a specified server. 
        /// If the client is only able to connect to one server, the previous connection will be closed.
        /// </para>
        /// <para>Checks if this client is able to connect to multiple servers</para>
        /// </summary>
        /// <param name="address">The IP or name from the server to connect to (i.e. "localhost" or "123.123.123.85")</param>
        /// <param name="port">The Port from the Server to connect (i.e. 5000)</param>
        /// <returns>Returns the Information for the Server-Connection as a NetPeer-Object</returns>
        public new NetPeer Connect(string address, int port)
        {
            if (m_allowMultipleServerConnections && GetPeers().Length > 0)
            {
                return base.Connect(address, port);
            }
            else
            {
                var peer = GetFirstPeer();

                if (peer?.ConnectionState == ConnectionState.Connected)
                {
                    CloseConnection(peer);
                }

                return base.Connect(address, port);
            }
        }
    }
}