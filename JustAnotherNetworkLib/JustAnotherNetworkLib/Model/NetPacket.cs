﻿using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using JustAnotherNetworkLib.Contracts;

namespace JustAnotherNetworkLib.Model
{
    /// <summary>
    /// Class with the purpose of containing data that will be send over Network.
    /// Contains functionality to Serialize and Deserialize containing data.
    /// </summary>
    public class NetPacket : INetPacket
    {
        /// <summary>
        /// The Message type of that packet represented by a single-byte (possible values: 0 - 255)
        /// </summary>
        public byte Type { get; private set; }

        /// <summary>
        /// The actual data that has to be sent. Can contain any possible object. But that Object should be serializeable.
        /// </summary>
        public object Payload { get; private set; }

        /// <summary>
        /// Create an empty NetPacket
        /// </summary>
        public NetPacket()
        {
            Type = 0b0000_0000;
            Payload = null;
        }

        /// <summary>
        /// Create a new NetPacket-Object with all the needed Information
        /// </summary>
        /// <param name="type">The Message-Type that is used for indication when the data is received</param>
        /// <param name="payload">The actual data that has to be sent. Can contain any possible object. But that Object should be serializeable.</param>
        public NetPacket(byte type, object payload)
        {
            Type = type;
            Payload = payload;
        }

        /// <summary>
        /// Serializes the data from the NetPacket-Object into a byte array that can easily be sent over Network.
        /// </summary>
        /// <returns>Returns the containing data as a byte-array</returns>
        public byte[] Serialize()
        {
            return AssembleData(GetPayloadDataAsByteArray());
        }

        /// <summary>
        /// Deserializes the given data and fills in the data for this object and returns it
        /// </summary>
        /// <param name="data">The previously serialized data as a byte-array</param>
        /// <returns>This NetPacket with the deserialized data</returns>
        public INetPacket Deserialize(byte[] data)
        {
            if (data == null) throw new System.ArgumentNullException(nameof(data));

            GetTypeData(data);
            GetPayloadData(data);

            return this;
        }

        #region Private Helpers

        /// <summary>
        /// Get the Payload-Data as a byte-Array
        /// </summary>
        /// <returns></returns>
        private byte[] GetPayloadDataAsByteArray()
        {
            // needed checks
            if (Payload?.GetType().IsSerializable == false)
            {
                throw new System.ArgumentException("The Payload should be serializable. When using a custom class, " +
                    "use the [System.Serializable] tag!", nameof(Payload));
            }
            if (Payload == null)
            {
                throw new System.ArgumentNullException(nameof(Payload));
            }

            var formatter = new BinaryFormatter();
            byte[] data = null;

            using (var memStream = new MemoryStream())
            {
                formatter.Serialize(memStream, Payload);
                data = memStream.ToArray();
            }

            return data;
        }

        /// <summary>
        /// Assembles the data in the following way:
        /// data = byte[] = yxxxxxxxxxxxxxxxxx...
        /// where y = the Type of the NetPacket (NetPacket.Type)
        /// where x = the Payload of the NetPacket (NetPacket.Payload)
        /// </summary>
        /// <param name="serializedPayload"></param>
        /// <returns></returns>
        private byte[] AssembleData(byte[] serializedPayload)
        {
            var data = new byte[serializedPayload.Length + 1];

            data[0] = Type;
            // copy in the payload data
            serializedPayload.CopyTo(data, 1);

            return data;
        }

        private void GetTypeData(byte[] data)
        {
            if (data.Length > 0)
                Type = data[0];
        }

        private void GetPayloadData(byte[] data)
        {
            if (data.Length <= 1)
            {
                Payload = null;
                return;
            }

            // copy the data into a new array
            var payloadData = new byte[data.Length - 1];

            for (int i = 1; i < data.Length; i++)
            {
                payloadData[i - 1] = data[i];
            }

            using (var memStream = new MemoryStream(payloadData))
            {
                var formatter = new BinaryFormatter();

                //memStream.Write(payloadData, 0, payloadData.Length);
                memStream.Seek(0, SeekOrigin.Begin);
                Payload = formatter.Deserialize(memStream);
            }
        }

        #endregion
    }
}