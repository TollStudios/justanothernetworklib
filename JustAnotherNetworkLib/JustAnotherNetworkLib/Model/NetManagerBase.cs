using LiteNetLib;
using LiteNetLib.Utils;
using JustAnotherNetworkLib.Utils;
using System;
using System.Linq;
using System.Threading;
using JustAnotherNetworkLib.Contracts;

namespace JustAnotherNetworkLib.Model
{
    /// <summary>
    /// Base-Class for the NetClient and NetServer with basic functionality
    /// </summary>
    public class NetManagerBase : IDisposable
    {
        /// <summary>
        /// Listenes On The Event that are occuring in the Net
        /// </summary>
        /// <value></value>
        public NetEventListener     EventListener { get; protected set; }

        public int Port { get; set; } = 5000;

        /// <summary>
        /// The shared connection-key
        /// </summary>
        public string ConnectionKey { get; internal set; }

        /// <summary>
        /// The Manager that manages stuffs on the Net
        /// </summary>
        protected NetManager        m_manager;

        /// <summary>
        /// A NetWriter that can write data on the Net
        /// </summary>
        protected NetDataWriter     m_dataWriter;

        /// <summary>
        /// The Thread that is responsible for polling events on the Net
        /// </summary>
        private Thread              m_managerPollThread;

        internal IDataCompressor     m_compressor;

        /// <summary>
        /// Create a new NetServer-Object with all the needed Information
        /// </summary>
        /// <param name="connectKey">A Connection-Key that has to be the same on the client and on the Server. Else a connection will not be made.</param>
        /// <param name="maxClients">The amount of clients that are able to connect to this Peer</param>
        /// <param name="updateTime">The time between every logic update. Is set to 15ms by default</param>
        /// <param name="discoveryEnabled">If this Peer can be "discovered". Is disabled by default</param>
        public NetManagerBase(string connectKey, int updateTime = 15, bool discoveryEnabled = false)
        {
            m_compressor = new BuildInDataCompressor();

            this.ConnectionKey = connectKey;

            m_dataWriter = new NetDataWriter();
            this.EventListener = new NetEventListener();

            m_manager = new NetManager(EventListener)
            {
                UpdateTime = updateTime,
                DiscoveryEnabled = discoveryEnabled
            };
        }

        #region Start Stop

        /// <summary>
        /// Starts the Manager.
        /// Will start the underlying logic and the updating of incoming 
        /// messages.
        /// </summary>
        public void Start()
        {
            m_manager.Start(this.Port);

            m_managerPollThread = new Thread(new ThreadStart(ManagerPollEvents));
            m_managerPollThread.Start();

            EventListener.OnClientStarted();
        }

        /// <summary>
        /// Stops the Manager.
        /// Will disconnect all currently connected peers and will stop
        /// the underlying functionality.
        /// </summary>
        public void Stop(string reason = "Manager is shutting down")
        {
            CloseConnection(reason);

            m_manager?.Stop();
            m_managerPollThread?.Abort();

            EventListener.OnClientStopped();
        }

        #endregion

        #region Establishing connections

        /// <summary>
        /// Connects to a specified endpoint.
        /// </summary>
        /// <param name="address">The IP or name from the server to connect to (i.e. "localhost" or "123.123.123.85")</param>
        /// <param name="port">The Port from the Server to connect (i.e. 5000)</param>
        /// <returns>Returns the new Connection as a NetPeer</returns>
        public NetPeer Connect(string address, int port)
        {
            return m_manager.Connect(address, port, this.ConnectionKey);
        }

        #endregion

        #region Closing connections

        /// <summary>
        /// Closes the connection for one specific peer
        /// </summary>
        /// <param name="peer">The peer to disconnect</param>
        /// <param name="reason">A reason for the disconnect</param>
        public void CloseConnection(NetPeer peer, string reason = "")
        {
            if (string.IsNullOrWhiteSpace(reason))
            {
                m_manager.DisconnectPeer(peer);
            }
            else
            {
                m_dataWriter.Reset();
                m_dataWriter.Put(reason);

                m_manager.DisconnectPeer(peer, m_dataWriter);
            }
        }

        /// <summary>
        /// Closes the connection to all currently connected peers
        /// </summary>
        /// <param name="reason">A reason for the disconnect</param>
        public void CloseConnection(string reason = "")
        {
            foreach (var p in GetPeers())
            {
                CloseConnection(p, reason);
            }
        }

        #endregion

        #region Providing Information

        /// <summary>
        /// Returns an Array of all currently connected Peers
        /// </summary>
        /// <returns>a NetPeer Array of all current connected Peers</returns>
        public NetPeer[] GetPeers()
        {
            return this.GetPeers(ConnectionState.Connected);
        }

        /// <summary>
        /// Returns an Array of all Peers with the specified connectionState
        /// </summary>
        /// <param name="connectionState"></param>
        /// <returns>a NetPeer Array with the Peers with the given state</returns>
        public NetPeer[] GetPeers(ConnectionState connectionState)
        {
            return m_manager.GetPeers(connectionState);
        }

        /// <summary>
        /// Get The first connected Peer
        /// </summary>
        /// <returns>Get The first connected Peer</returns>
        public NetPeer GetFirstPeer()
        {
            return m_manager.FirstPeer;
        }

        /// <summary>
        /// Get the statistics for all connections
        /// </summary>
        /// <returns>NetStatistics object with the information</returns>
        public NetStatistics GetStatistics()
        {
            return m_manager.Statistics;
        }

        #endregion

        #region Sending Messages

        /// <summary>
        /// Send a message to ONE specific Peer
        /// </summary>
        /// <param name="receiver">The Peer to send a message to</param>
        /// <param name="id">The id for the message</param>
        /// <param name="payload">The payload for the message</param>
        /// <exception cref="System.ArgumentOutOfRangeException">
        /// Thrown when the provided receiver could not be found.
        /// </exception>
        public void SendMessage(NetPeer receiver, byte id, object payload)
        {
            SendMessage(receiver, new NetPacket(id, payload));
        }

        /// <summary>
        /// Send a message to ONE specific Peer
        /// </summary>
        /// <param name="receiver">The Peer to send a message to</param>
        /// <param name="message">The Message to send</param>
        /// <exception cref="System.ArgumentOutOfRangeException">
        /// Thrown when the provided receiver could not be found.
        /// </exception>
        public void SendMessage(NetPeer receiver, NetPacket message)
        {
            if (GetPeers().ToList().Contains(receiver))
            {
                m_dataWriter.Reset();
                m_dataWriter.Put(m_compressor.Compress(message.Serialize()));

                receiver.Send(m_dataWriter, DeliveryMethod.Sequenced);
            }
            else
            {
               throw new ArgumentOutOfRangeException("The provided peer on " +
                $"{receiver.EndPoint} could not be found!");
            }
        }

        /// <summary>
        /// Send a message to ALL Peers
        /// </summary>
        /// <param name="id">The id for the message</param>
        /// <param name="payload">The payload for the message</param>
        /// <param name="except">Optional: To avoid sending a message to a specific peer</param>
        public void SendMessage(byte id, object payload, NetPeer except = null)
        {
            SendMessage(new NetPacket(id, payload), except);
        }

        /// <summary>
        /// Send a message to ALL Peers
        /// </summary>
        /// <param name="message">The Message to send</param>
        /// <param name="except">Optional: To avoid sending a message to a specific peer</param>
        public void SendMessage(NetPacket message, NetPeer except = null)
        {
            foreach(var p in GetPeers())
            {
                if (except != null && except.EndPoint == p.EndPoint) { continue; }
                else
                {
                    SendMessage(p, message);
                }
            }
        }

        #endregion 

        #region Other

        /// <summary>
        /// Dispose this object (IDisposable)
        /// </summary>
        public void Dispose()
        {
            Stop();

            m_manager = null;
            m_managerPollThread = null;
        }

        /// <summary>
        /// Simple ongoing Loop that will Poll all the Events for the 
        /// NetManager to be handled. Will just stop, when the Manager
        /// is not running anymore
        /// </summary>
        private void ManagerPollEvents()
        {
            while (m_manager.IsRunning)
            {
                m_manager.PollEvents();
            }
        }

        #endregion

    }
}