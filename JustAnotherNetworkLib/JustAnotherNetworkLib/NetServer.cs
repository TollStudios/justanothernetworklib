﻿using JustAnotherNetworkLib.Model;

namespace JustAnotherNetworkLib
{
    /// <summary>
    /// Class to handle the connection to client (or from a client) to a server as a server
    /// </summary>
    public class NetServer : NetManagerBase
    {
        /// <summary>
        /// Create a new NetServer-Object with all the needed Information.
        /// </summary>
        /// <param name="connectKey">A Connection-Key that has to be the same on the client and on the Server. Else a connection will not be made.</param>
        /// <param name="maxClients">[OPTIONAL] The Amount of Clients that can connect to this server. Is set to 99 by default.</param>
        /// <param name="updateTime">[OPTIONAL] Sets the interval in which the logic of the underlying Library will be updated in Milliseconds. Is set to 15ms by default.</param>
        /// <param name="discoveryEnabled">[OPTIONAL] If this server can be "discovered" by other Clients. Is disabled by default</param>
        /// <returns></returns>
        public NetServer(string connectKey, int updateTime = 15, bool discoveryEnabled = false) : base(connectKey, updateTime, discoveryEnabled)
        {
            // Everything's handled in the base constructor
        }
    }
}